<?php
    session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="./loginstyle.css" type="text/css">
    <title>Raspored - Login</title>
  </head>
  <body>
    <form action="includes/login.inc.php" method="POST" id="loginforma">
      <div class="form-group">
        <label>Korisničko ime</label>
        <input type="text" name="username" class="form-control">
      </div>
      <div class="form-group">
        <label>Lozinka</label>
        <input type="password" name="password" class="form-control">
      </div>
      <button type="submit" name="submit" class="btn btn-primary">Prijava</button>
      <?php
            $fullUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            if(strpos($fullUrl,"login=invalidUsername")){
                echo "<br> <small style='color:red'> Nepostojeće korisničko ime. </small> <br>";
            }
            elseif(strpos($fullUrl,"login=invalidPassword")){
                echo "<br> <small style='color:red'> Pogrešna lozinka. </small> <br>";
            }
            elseif(strpos($fullUrl,"logout=true")){
                echo "<br> <small style='color:green'> Uspješno ste odjavljeni. </small> <br>";
            }
      ?>
    </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  </body>
</html>