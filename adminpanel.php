<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="./adminpanelstyle.css" type="text/css">
    <title>Raspored-Admin Panel</title>
  </head>
  <body>
    <form action="includes/adminpanel.inc.php" method="POST" id="adminpanel">
      <div class="form-group">
        <label>Korisničko ime</label>
        <input type="text" name="username" class="form-control">
      </div>
      <div class="form-group">
        <label> Stara lozinka</label>
        <input type="password" name="password" class="form-control">
      </div>
      <div class="form-group">
        <label> Nova lozinka</label>
        <input type="password" name="newpassword" class="form-control">
      </div>
      <div class="form-group">
        <label> Ponovite novu lozinku</label>
        <input type="password" name="confirmnewpassword" class="form-control">
      </div>
      <div class="form-group form-check">
      </div>
      <button type="submit" name="submit" class="btn btn-primary">Spremi</button>
      <?php
            $fullUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            if(strpos($fullUrl,"adminPanel=invalidUsername")){
                echo "<br> <small style='color:red'> Nepostojeći korisnik. </small> <br>";
            }
            elseif(strpos($fullUrl,"adminPanel=invalidPassword")){
                echo "<br> <small style='color:red'> Pogrešna lozinka. </small> <br>";
            }
            elseif(strpos($fullUrl,"adminPanel=passwordMismatch")){
                echo "<br> <small style='color:red'> Nova lozinka i ponovljena nova lozinka se ne podudaraju. </small> <br>";
            }
            elseif(strpos($fullUrl,"adminPanel=success")){
                echo "<br> <small style='color:green'> Promjene su spremljene. </small> <br>";
            }
      ?>
    </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  </body>
</html>