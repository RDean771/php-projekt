<?php
    include_once 'dbh.inc.php';
    if(!isset($_POST['submit'])){
        header("Location: ../adminpanel.php");
    } else {
        $username=mysqli_real_escape_string($conn,$_POST['username']);
        $password=mysqli_real_escape_string($conn,$_POST['password']);
        $newPassword=mysqli_real_escape_string($conn,$_POST['newpassword']);
        $confirmNewPassword=mysqli_real_escape_string($conn,$_POST['confirmnewpassword']);
        $sql = "SELECT * FROM admins WHERE username='$username'";
        $result = mysqli_query($conn,$sql);
        $resultCheck = mysqli_num_rows($result);
        if ($resultCheck < 1){
            header("Location: ../adminpanel.php?adminPanel=invalidUsername");
        }
        else{
            if ($row = mysqli_fetch_assoc($result)){
                $hashedPwCheck = password_verify($password,$row['password']);
                if($hashedPwCheck == false){
                    header("Location: ../adminpanel.php?adminPanel=invalidPassword");
                }
                elseif($hashedPwCheck == true){
                    if($newPassword != $confirmNewPassword){
                        header("Location: ../adminpanel.php?adminPanel=passwordMismatch");
                    }
                    else{
                        $hashedPw = password_hash($newPassword, PASSWORD_DEFAULT);
                        $sql = "UPDATE admins SET password ='$hashedPw' WHERE username='$username'";
                        mysqli_query($conn,$sql);
                        header("Location: ../adminpanel.php?adminPanel=success");
                    }
                }
            }
        }
    }
    
?>