<?php
    include_once 'dbh.inc.php';
    session_start();
    if(!isset($_POST['submit'])){
        header("Location: ../index.php?login=error");
    } else {
        $username=mysqli_real_escape_string($conn,$_POST['username']);
        $password=mysqli_real_escape_string($conn,$_POST['password']);
        $sql = "SELECT * FROM admins WHERE username='$username'";
        $result = mysqli_query($conn,$sql);
        $resultCheck = mysqli_num_rows($result);
        if ($resultCheck < 1){
            header("Location: ../index.php?login=invalidUsername");
        }
        else{
            if ($row = mysqli_fetch_assoc($result)){
                $hashedPwCheck = password_verify($password,$row['password']);
                if($hashedPwCheck == false){
                    header("Location: ../index.php?login=invalidPassword");
                }
                elseif($hashedPwCheck == true){
                    $_SESSION['username'] = $row['username'];
                    $_SESSION['password'] = $row['password'];
                    header("Location: ../welcome.php");
                }
            }
        }
    }
    
?>